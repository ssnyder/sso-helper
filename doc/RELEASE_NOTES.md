# SSO Helper

## tdaq-11-00-00

The `sso-helper` library help to interact with CERN Single Sign On (SSO) system. The code has been extracted from various existing private implementations (most notably `daq_tokens`) to make it more generally available.

The main motivation is to avoid having to call out to the existing Python scripts like `auth-get-sso-cookies` and `auth-get-sso-token` from DAQ applications. Apart from the overhead of starting a Python interpreter for this task, the scripts are either incompatible with the TDAQ environment (if taken from the system) or not always up to date (if take from LCG).

Two command line tools replicate (and extend) the functionality of the above mentioned scripts and can be used from shell scripts:

    get-sso - retrieve an SSO protected URL, optinally store cookies for re-use
    get-sso-token - retrieve (new) SSO OICD tokens, refresh them etc.

For more details see the [README](https://gitlab.cern.ch/atlas-tdaq-software/sso-helper) file.
